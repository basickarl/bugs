"use strict";

var koa = require('koa');
var http = require('http');
var router = require('koa-router');
var handlebars = require("koa-handlebars");
var serve = require("koa-static"); // static reserved word
var mount = require("koa-mount");

var app = koa();
app.use(serve(__dirname + '/public')); // important that it is above the rest

var mainRouter = new router(); // signed in and non signed in users
mainRouter.use(handlebars({
    defaultLayout: 'main',
    layoutsDir: 'layouts',
    viewsDir: 'views',
    partialsDir: 'partials'
}));
mainRouter
    .get('/', function* () {
        yield this.render('home', {});
    })
    .get('/signout', function* () {
        this.response.redirect('/out'); // redirect to login
    })    
    .get('/out', function* () {
        yield this.render('out', {});
    });
app.use(mount('/', mainRouter.routes())); // tell the app which router to use

var server = app.listen(8080, function () {
    console.log('listening on port 8080');
});