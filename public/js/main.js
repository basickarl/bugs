'use strict';

angular.module('myApp', [])
    .config(['$locationProvider', function ($locationProvider) {
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');
    }])
    .controller('homeCtrl', [function () {
        console.log('home');
    }])
    .controller('outCtrl', [function () {
        console.log('out');
    }])
    .controller('otherCtrl', [function () {
        console.log('something else');
    }]);